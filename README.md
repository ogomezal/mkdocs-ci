Mkdocs Gitlab Pages CI Configuration
====================================

This repository holds Gitlab CI template which, once included, builds the MkDocs documentation artifacts and pushes it to S3.
These static files are then served by special infrastructure deployed on OKD4.

Example:

```yaml
include:
  - project: 'authoring/documentation/mkdocs-ci'
    file: 'mkdocs-gitlab-pages.gitlab-ci.yml'
```
